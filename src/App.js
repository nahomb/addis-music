import MusicCard from './components/musicCard'
import MainLayout from './layout/mainLayout'; 
import AddButton from './components/Button'
import {useEffect, useState} from 'react'
import CreateMusic from './components/CreateMusic'
import { css } from '@emotion/css';
import {useDispatch, useSelector} from 'react-redux'
import {get_musics_filter,get_musics} from './redux/actions/musicActions'
import Stat from './components/statisticsCard'
import {get_stat,get_stat_filter} from './redux/actions/statActions'
import axios from './axios'

export const selectstyle = css`
border:none;
appearance:none;
background:#f2f2f2;
padding:8px;
border-radius:3px;
width:200px; 
margin:4px;
outline:none;  
`
export const statArea = css`margin:10px; border-radius:10px;background-color:white; display:flex; padding:10px; align-items:center; justify-content: space-between; 
box-shadow: 0px 0.1rem 0.2rem grey;`

const App= () => {
  const [isDisabled, setIsDisabled] = useState(false);
  const [openCreate, setOpenCreate] = useState(false); 
  const dispatch = useDispatch()
  const musics = useSelector(state=> state.musicReducer.musics)
  const artist = useSelector(state=> state.musicReducer.artist)
  const stat = useSelector(state=> state.statReducer.stat)
  const genre = useSelector(state=> state.musicReducer.genre)
  useEffect(()=>{
    dispatch(get_musics());
    dispatch(get_stat());
  },[]) 

  const addButtonHandler = () =>{
    setOpenCreate(!openCreate)
  }

  const handleFilter = async(e) =>{
    try{
      dispatch(get_musics_filter(`${e.target.name}=${e.target.value}`));
      dispatch(get_stat_filter(`${e.target.name}=${e.target.value}`));
    }catch(err){

    }
  }
  console.log('artsit s ', stat)
  if(!musics){
    return <div> Loading...</div>
  }
    return (
    <div className="App">
       <MainLayout>
         <header className={css`display:flex; align-items:center; justify-content:space-between;`}>
          <div className={css`display:flex`}>
            <select name="artist" className={selectstyle} onChange={handleFilter}>
            <option value="">--  Filter By Artist --</option>
             
               {
                musics.length>0? artist.map(el=>{
                  return  <option value={el} >{el}</option>
                }):''
               }
            </select>
            <select name="genre" onChange={handleFilter} className={selectstyle}>
            <option value="" >-- Filter By Genre --</option>
            {
                musics.length>0? genre.map(el=>{
                  return  <option value={el}>{el}</option>
                }):''
               }
            </select>
          </div>
           <AddButton clickHandler={addButtonHandler} isDisabled={isDisabled} title="Add Music"/>
         </header>
         
         {
          openCreate? <CreateMusic openOrCloseCreate={addButtonHandler}/> :""
        }
        <div >
          {
            stat? ( <div className={statArea}>
              <Stat title="All Songs" value={stat.songs}/>
              <Stat title="All Albums" value={stat.album}/>
              <Stat title="All Artist" value={stat.genre}/>
              </div>) : <div>Loading</div>
          }
         
        
        </div>

    <div className={css`display:flex; flex-wrap:wrap;`}>
 
        {
          musics || musics.length > 0 ?  musics.map(el=> ( <MusicCard music={el}/> )) : 
          <h1>no Data</h1>
        }
     </div>

       </MainLayout> 
    </div>
  );
} 

export default App;
