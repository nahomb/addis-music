import { css } from "@emotion/css";

export const navbar = css` 
    height: 60px;
    width:77%;
    z-index:50;
    display:flex;
    align-items: center;
justify-content: space-between;
    position: sticky;
    top:0; 
    padding: 2px 10rem 2px 10rem;
    box-shadow: 0px 0.1rem 0.2rem grey;
`
export const logo = css`
    max-height:60px;
`
export const footer = css` 
 position: absolute;
 bottom:0; 
 width:77%;
 display:flex;
justify-content: space-between;
 padding: 4px 10rem 4px 10rem;
 background-color:rgb(201,215,16);
`
export const footerText = css`
 font-size:12px;
 font-weight:bold;
`
export const main = css`

padding: 4px 10rem 4px 10rem; 
 
background-color: rgb(249,248,247); 
height:85vh;
overflow:scroll;
`

export const sidebar = css`

`
export const MainLayout = ({children})  =>{
    return (<div >
        <div className={navbar}>
        <div className={css`display:flex; align-items:center`}>
        <img className={logo} src="https://media.licdn.com/dms/image/C4E0BAQHurifakT9r2g/company-logo_200_200/0/1658769689927?e=2147483647&v=beta&t=zb-Tkcb83nWllnPe_sB5d6I_AECqvnMMuNQpyWuqzis" alt="logo"/>
        <span className={css`font-weight:700;`}>Music</span>
        </div>
          </div>
          <div className={main}>
            {children}
          </div>
          <footer className={footer}>
            <div  className={footerText}>@Addis Software Test Project</div>
            <div className={footerText}>Developer: Nahom Balcha</div>
          </footer>
          </div>)
}

export default MainLayout;