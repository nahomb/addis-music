import { css } from "@emotion/css";

export const statCard = css`
    border-radius:10px;
    padding:20px;
    background-color:#F98937;
    color:white;
    font-weight:bold;
    display:flex;
    flex-direction:column;
    align-items:center;
    justify-content:center
    margin:20px;

`

const statisticsCard = (props) =>{
    return (
        <div className={statCard}>
            <div>{props.title}</div>
            <div>{props.value}</div>
        </div>
    )
}

export default statisticsCard;