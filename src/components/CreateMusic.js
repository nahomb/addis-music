import { css } from "@emotion/css";
import Save from './Button'
import {useEffect, useState} from 'react'
import axios from './../axios'
import { useDispatch} from 'react-redux'
import {get_musics} from './../redux/actions/musicActions'
import { get_stat } from "../redux/actions/statActions";

const createComp = css`
    height: 98%;
    width: 25rem; 
    position:absolute;
    top:0;
    right:0;
    z-index:60;
    background-color: white;  
    border-radius: 10px; 
    box-shadow: 0px 1rem 1.5rem grey;
`

const title = css`
color: #ffffff;
background-color: #3093c7;
font-weight:bold;
padding:20px;
`
const input = css`
border:none;
appearance:none;
background:#f2f2f2;
padding:12px;
border-radius:3px;
width:250px; 
margin:4px;
outline:none;  
`
const CreateMusic = (props) =>{

    const [isDisabled, setIsDisabled] = useState(false);
    const [form, setForm] = useState({})
    const dispatch = useDispatch();

    useEffect(()=>{
         
        if(props.music){
           setForm({
                ...form,
                title:props.music.title,
                artist: props.music.artist,
                album :props.music.album,
                genre: props.music.genre
            }) 
           
        }
    },[])

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name] : e.target.value
        })
    }
    const addButtonHandler = async (e) =>{
        try{
            if(props.music){
                await axios.patch(`/songs/${props.music._id}`,form).then(res=>{
                    dispatch(get_musics())
                });
            }else{
                await axios.post('/songs',form).then(res=>{
                    dispatch(get_musics())
                    dispatch(get_stat())
                });
            }

           

            props.openOrCloseCreate();
        }catch(err){

        }

        
    }
    return <div className={createComp}>
        <div className={title}>Create Music {form.title+'what'}</div>
        <div className={css`display:flex; align-items:center; flex-direction:column; margin-top:10px;`}>
        <input name="title" value={form.title} onChange={handleChange} className={input} type='text' placeholder="Title"/>
        <input name="artist" value={form.artist} onChange={handleChange} className={input} type='artist' placeholder="Artist"/>
        <input name="genre" value={form.genre} onChange={handleChange} className={input} type='genre' placeholder="Genre"/>
        <input name="album" value={form.album} onChange={handleChange} className={input} type='album' placeholder="Album"/>
        <Save clickHandler={addButtonHandler} isDisabled={isDisabled} title="Save"/>
          </div> 
    </div>
}

export default CreateMusic;