import {css} from '@emotion/css'

const buttonStyle = css`
display: block;
width: 150px; 
margin-top: 20px;
border: 1px solid #25729a;
font-weight:600;
border-radius: 3px;
text-align: center;
color: #ffffff;
background-color: #3093c7;
margin-bottom: 20px;
padding: 7px 7px 7px 7px;
cursor:pointer;
`
const button = (props) =>{
    return <button onClick={props.clickHandler} disabled={props.isDisabled} className={buttonStyle}>
        {props.title}
    </button>
}


export default button;