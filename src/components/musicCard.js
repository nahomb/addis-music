import {css} from "@emotion/css";
import {FaTrash, FaPaintBrush} from 'react-icons/fa'
import axios from './../axios'
import { useDispatch } from "react-redux";
import {get_musics} from './../redux/actions/musicActions'
import UpdateMusic from './CreateMusic'
import {useState} from 'react'
export const MCard =css`
@font-face: {
    font-family: 'font1';
    src: url("https://fonts.googleapis.com/css?family=Montserrat:400,400i,700");
};
background-color: white;
max-width: 210px;
display: flex;
flex-direction: column;
overflow: hidden;
border-radius: 2rem;
box-shadow: 0px 1rem 1.5rem rgba(black, 0.5);
box-shadow: 0 3px 10px gray;
margin:10px;
`;

export const Banner = css`
background-image: url(https://images.unsplash.com/photo-1481207801830-97f0f9a1337e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80);
background-position: center; 
background-size: cover;
height: 8rem;
display: flex;
align-items: flex-end;
justify-content: center;
box-sizing: border-box;

`

export const Menu = css`
width: 100%;  
display: flex; 
align-items: flex-start;
justify-content: flex-end;
position: relative; 
box-sizing: border-box;`

export const name = css`
    text-align: center;
    padding: 0; padding-size: 0.5rem;
    margin: 0
`
export const title = css `
color:lighten(font-color, 50%);
    font-size: 0.85rem;
    font-weight:500;
    text-align: center;
    padding: 0 ; padding-size: 1.2rem
`
export const desc = css`
text-align: justify;
padding: 10px; padding-size: 2.5rem;
font-size:12px;
font-weight:600; 
order: 100`

export const actions = css`
cursor:pointer;
padding:5px;
`
 
const MusicCard = ({music}) =>{

  const dispatch = useDispatch();
  const [updateOpen, setUpdateOpen] = useState(false)

  const openOrCloseCreate = () =>{
    setUpdateOpen(!updateOpen)
  }
  const deleteMusic = async() =>{
  
    try{
      await axios.delete(`/songs/${music._id}`).then(res=>{
        dispatch(get_musics())
      })
    }catch(err){

    }
  }

    return (
    
<div className={MCard}>
  <div className={Banner}>
  </div>
  <div className={Menu}>
    <div class="opener"><span></span><span></span><span></span></div>
  </div>
  <h3 className={name}>{music.title}</h3>
  <div className={title}>{music.artist}</div>
  <div className={css`display:flex; align-items:center; justify-content:center; padding:5px;`}>
    <FaTrash onClick={deleteMusic} color="red" className={actions}/>
    <FaPaintBrush color="blue"  className={actions} onClick={openOrCloseCreate}/>
  </div>
  <div className={desc}>Morgan has collected ants since they were six years old and now has many dozen ants but none in their pants.</div>
     { updateOpen ?<UpdateMusic music={music}  openOrCloseCreate={openOrCloseCreate}/> :""}  
</div> 
            )
}

export default MusicCard;