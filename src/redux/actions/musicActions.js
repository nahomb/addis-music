import axios from './../../axios'
import {GET_MUSIC, ART_FILTER_OPTION,GEN_FILTER_OPTION} from './../Types'

export const get_musics= () =>{
    return async (dispatch) =>{
        try{
            const res = await axios.get('/songs');
             console.log('response ',res)
            dispatch({
                type:GET_MUSIC,
                payload:res.data.data.data
            })

            dispatch({
                type:ART_FILTER_OPTION,
                payload: res.data.data.data.map(el=> el.artist)
            })
            dispatch({
                type:GEN_FILTER_OPTION,
                payload: res.data.data.data.map(el=> el.genre)
            })
        }catch(err){
            console.log(err)
        }
        
    }
}
 
export const get_musics_filter= (filter) =>{
    return async (dispatch) =>{
        try{
            const res = await axios.get(`/songs?${filter}`);
             console.log('response ',res)
            dispatch({
                type:GET_MUSIC,
                payload:res.data.data.data
            })
        }catch(err){
            console.log(err)
        }
        
    }
}
