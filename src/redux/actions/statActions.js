import axios from './../../axios'
import {GET_MUSIC, ART_FILTER_OPTION,GEN_FILTER_OPTION, GET_STAT} from './../Types'

export const get_stat= () =>{
    return async (dispatch) =>{
        try{
            const res = await axios.get('/songs/count');
      
            dispatch({
                type:GET_STAT,
                payload:res.data.message
            })

            
        }catch(err){
            console.log(err)
        }
        
    }
}
 
export const get_stat_filter= (filter) =>{
    return async (dispatch) =>{
        try{
            const res = await axios.get(`/songs/count?${filter}`);
             console.log('response ',res)
            dispatch({
                type:GET_STAT,
                payload:res.data.message
            })
        }catch(err){
            console.log(err)
        }
        
    }
}
