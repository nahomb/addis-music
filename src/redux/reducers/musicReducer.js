import {GET_MUSIC,ART_FILTER_OPTION,GEN_FILTER_OPTION} from '../Types'

const initialState = {
    musics:[],
    artist:[],
    genre:[],
    music: undefined
}

const userReducer = (state=initialState, action) =>{

    const {type, payload} = action;

    switch(type){
        case GET_MUSIC:
            return {
                ...state,
                musics: payload
            }
            break;
        case ART_FILTER_OPTION:
            return {
                ...state,
                artist: payload
            }
            break;
        case GEN_FILTER_OPTION:
                return {
                    ...state,
                    genre: payload
                }
              break;
        default:
            return state;
    }
}

export default userReducer;
