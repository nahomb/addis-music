import {GET_MUSIC,ART_FILTER_OPTION,GEN_FILTER_OPTION, GET_STAT} from '../Types'

const initialState = {
    stat:[],
    music: undefined
}

const statReducer = (state=initialState, action) =>{

    const {type, payload} = action;

    switch(type){
        case GET_STAT:
            return {
                ...state,
               stat:payload
            } 
              break;
        default:
            return state;
    }
}

export default statReducer;
