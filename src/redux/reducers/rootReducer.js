import { combineReducers } from 'redux'
import musicReducer from './musicReducer'
import statReducer from './statReducer';
const rootReducer = combineReducers({
    musicReducer,
    statReducer
})

export default rootReducer;
