import axios from 'axios'

export default axios.create({
    baseURL:'https://addis-music-api.onrender.com',
    headers:{
        "Content-Type":"application/json"
    }
})
